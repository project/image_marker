
INTRODUCTION
------------

This module provides and adapts the jQuery plugin as a Drupal form element for a quiz of matching.
See more for the plugin into the link.
https://www.jqueryscript.net/other/Drag-Drop-Image-Marker-Plugin-jQuery.html


INSTALLATION / CONFIGURATION
-------------------

 * Download image_marker from https://github.com/bear926/image_marker.
    Unzip it in the sites/all/libraries/image_marker directory
    (jquery.image-marker.js should be located in sites/all/libraries/image_marker/src/jquery.image-marker.js).

* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


HOW TO USE IT
-------------------
This module provide two mode. Admin mode and user answer mode.
Default value Admin mode.
Default Element configuration:

'#answer_mode' => FALSE,
'#drag_disabled' => FALSE,
'#close_disabled' => FALSE,


'#default_value' => array(
  'side_left' => array (
    0 => array (
      'content' => 'My text',
      'className' => 'green',
      'col' => '1',
      'pos' => array (
        'x' => '0',
        'y' => '33',
      ),
    ),
  ),
  'side_right' => array (
    0 => array (
      'className' => 'yello',
      'drag_disabled' => 'true',
      'col' => '2',
      'pos' => array (
        'x' => '796',
        'y' => '24',
       ),
    ),
    1 => array (
      'className' => 'yello',
      'drag_disabled' => 'true',
      'col' => '2',
      'pos' => array (
        'x' => '796',
        'y' => '103',
      ),
    ),
  ),
),

You can edit and redefine form element example with form_alter.
  $form['image_marker_' . $key]['action']['add_side_right']['#access'] = FALSE;
  $form['image_marker_' . $key]['action']['add_side_left']['#access'] = FALSE;
  $form['image_marker_' . $key]['action']['save']['#submit'][] = 'ca_send_lab_image_marker_form_submit';
  $form['image_marker_' . $key]['action']['save']['#executes_submit_callback'] = TRUE;