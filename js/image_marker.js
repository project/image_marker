(function($) {

  Drupal.image_marker = Drupal.image_marker || {};
  /**
   * Attaches the image_marker behavior to each image_marker form element.
   */
  Drupal.behaviors.image_marker = {
    attach: function (context, settings) {
      $('.form-type-image-marker', context).once('image-marke-init', function () {
        var $this = $(this);
        var imageMarker = $this.find('.image-marker-work-element');
        var image_marker_class = $this.find('.image-marker-work-element-action')
          .attr('id')
          .replace('action', 'image-marker-work-element');

        // find form_build_id
        var form = imageMarker.parents('form');
        if (form) {
          var form_build_id = form.find('[name="form_build_id"]').val();
          var newlastdata = sessionStorage.getItem(form_build_id+image_marker_class);
          var lastdata = null;
          if (newlastdata) {
            lastdata = JSON.parse(newlastdata);
          }
        }
        var next_value = settings.image_marker[image_marker_class];
        // build plugin object
        imageMarker.imageMarker({
          drag_disabled: next_value.drag_disabled,
          answer_mode: next_value.answer_mode,
          close_disabled: next_value.close_disabled
        });

        // build default marker
        if (!!lastdata) {
          lastdata.forEach(function (m) {
            $(imageMarker).trigger('add_marker', m);
          });
          $this.prepend(markerMessages(Drupal.t('Save changes before submit.'), 'warning', $this));
        }
        else if (!!next_value.default) {
          next_value.default.forEach(function (m) {
            $(imageMarker).trigger('add_marker', m);
          });
        }

        // add right marker
        $this.find('.add_right_marker').click(function () {
          $(imageMarker).trigger('add_marker', {
            className: 'yello',
            drag_disabled: true,
            col: 2
          });
          $this.prepend(markerMessages(Drupal.t('Save changes before submit.'), 'warning', $this));
        });

        // add left marker
        $this.find('.add_left_marker').click(function () {
          $(imageMarker).trigger('add_marker', {
            className: 'green',
            col: 1
          });
          $this.prepend(markerMessages(Drupal.t('Save changes before submit.'), 'warning', $this));
        });

        // show message
        $this.find('.image_marker_save').click(function () {
          $this.prepend(markerMessages('Changes saved.', 'status', $this));
        });

        // attach to ajax save
        if ($this.find('.image_marker_save').length !== 0) {
          var id_image_marker_save = $this.find('.image_marker_save')[0].id;
          var ajaxbeforeSerialize = Drupal.ajax[id_image_marker_save].beforeSerialize;
          Drupal.ajax[id_image_marker_save].beforeSerialize = function (element , options) {
            insertInput(imageMarker , form);
            ajaxbeforeSerialize.call(this , element , options);
          };
        }

        // add extra option
        if (next_value.answer_mode) {
          $('.image-marker__text-box__title').attr('contenteditable', 'false');
          $('.image-marker__text-box__content').attr('contenteditable', 'false');
        }
      });

      var imageMarkers = $('.image-marker-work-element');
      var forms = imageMarkers.parents('form');
      // add global save marker item
      forms.submit(function() {
        $this = $(this);
        var imageMarker = $this.find('.image-marker-work-element');
        insertInput(imageMarker, $this);
      });

      // insert input field
      function insertInput(imageMarker, form) {
        for (i = 0; i < imageMarker.length; i++) {
          var current = $(imageMarker[i]);
          var elements_name = current.data('name');
          current.trigger('get_markers' , function (data) {
            for (k = 0; k < data.length; k++) {
              var field = $('<input></input>');
              field.attr('type' , 'hidden');
              field.attr('name' , elements_name + '[' + k + ']');
              field.attr('value' , JSON.stringify(data[k]));
              form.append(field);
            }
            // sessionStorage
            var form_build_id = form.find('[name="form_build_id"]').val();
            var image_marker_class = form.find('.image-marker-work-element-action')
              .attr('id')
              .replace('action', 'image-marker-work-element');
            if (form_build_id) {
              sessionStorage.setItem(form_build_id+image_marker_class, JSON.stringify(data));
            }
          });
        }
      }

      // show info message
      function markerMessages(text, type, $this) {
        var message = $this.find('.tabledrag-changed-warning');
        if (message) {
          message.remove();
        }
        return $('<div class="tabledrag-changed-warning messages ' + type + '">' + text + '</div>');
      }
    }
  };
})(jQuery);